from django.shortcuts import render, redirect
from .forms import ProductForm
from .models import Product
from django.utils import timezone

# Create your views here.
def home(request):
    if request.method == "POST":
        form = ProductForm(request.POST)
        if form.is_valid():
            product = Product()
            product.name = form.cleaned_data['name']
            product.price = form.cleaned_data['price']
            product.rating = form.cleaned_data['rating']
            product.save()
        return redirect('/')
    else:
        product_count = Product.objects.count()
        form = ProductForm()
        products = Product.objects.all().order_by('-time_added')
        response = {'products' : products, 'form': form, 'title':'Carrot by Cebet','count':product_count}
        return render(request, 'app/home.html', response)