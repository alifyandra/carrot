# Soo... Carrot? 🥕

Hi! this is just a personal project on a dummy store website using the **Django** framework.

# Features

-   **Fruity Colors!** 💓🥕 🍆 🍓
-   A navbar
-   **Django Models** & **Forms** powered
-   Buy panel
-   Sell panel
-   Search bar
-   **Colorful** animated background 🤩

# Links

-   [Gitlab Remote Repo](https://gitlab.com/alifyandra/carrot)
-   [Website](https://carrotstore.herokuapp.com/)

# Stats

Heroku [![pipeline status](https://gitlab.com/alifyandra/carrot/badges/master/pipeline.svg)](https://gitlab.com/alifyandra/carrot/commits/master)
